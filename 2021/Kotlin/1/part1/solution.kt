import java.io.File
import java.io.BufferedReader

fun count_increments_in_value(array: List<Int>) : Int {
  var count: Int = 0
  array.forEachIndexed { idx, item ->
    if (idx > 0 && item > array[idx - 1])
      count += 1
  }
  return count
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n").map { it -> it.toInt() }
  val increments = count_increments_in_value(array)
  println(increments)
}