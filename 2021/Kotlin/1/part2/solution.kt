import java.io.File
import java.io.BufferedReader

fun sum_of_items_around(array: List<Int>, index: Int) : Int {
 return array[index - 1] +  array[index] +  array[index + 1]
}

fun count_increments_in_sums(array: List<Int>) : Int {
  var count: Int = 0
  array.forEachIndexed { idx, item ->
    if (idx > 1 && idx < array.count() - 1) {
      var sum1 = sum_of_items_around(array, idx - 1)
      var sum2 = sum_of_items_around(array, idx)
      if (sum2 > sum1)
        count += 1
    }
  }
  return count
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n").map { it -> it.toInt() }
  val increments = count_increments_in_sums(array)
  println(increments)
}