import java.io.File
import java.io.BufferedReader

fun get_distance(array: List<List<String>>) : Int {
  var distances = mutableMapOf("forward" to 0, "down" to 0, "up" to 0)
  array.forEach { item ->
    distances.put(item[0], distances[item[0]]!! + item[1].toInt())
  }
  return distances["forward"]!! * (distances["down"]!! - distances["up"]!!)
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n").map { it.split(" ") }
  println(get_distance(array))
}