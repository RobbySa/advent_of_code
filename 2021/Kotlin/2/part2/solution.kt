import java.io.File
import java.io.BufferedReader

fun get_fixed_distance(array: List<List<String>>) : Int {
  var distances = mutableMapOf("forward" to 0, "aim" to 0, "depth" to 0)
  array.forEach { item ->
    when (item[0]) {
      "forward" -> {
        distances.put("forward", distances["forward"]!! + item[1].toInt())
        distances.put("depth", distances["depth"]!! + (distances["aim"]!! * item[1].toInt()))
      }
      "down" -> distances.put("aim", distances["aim"]!! + item[1].toInt())
      "up" -> distances.put("aim", distances["aim"]!! - item[1].toInt())
    }
  }
  return distances["forward"]!! * distances["depth"]!!
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n").map { it.split(" ") }
  println(get_fixed_distance(array))
}