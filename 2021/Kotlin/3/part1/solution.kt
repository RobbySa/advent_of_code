import java.io.File
import java.io.BufferedReader
import kotlin.math.pow

fun binary_to_int(rates: List<String>) : List<Int> {
  var converted_rates = mutableListOf(0, 0)
  rates.forEachIndexed { i, binary ->
    binary.reversed().forEachIndexed { j, bit ->
      converted_rates.set(i, converted_rates[i] + (Character.getNumericValue(bit) * (2.0.pow(j).toInt())))
    }
  }
  return converted_rates
}

fun get_gamma_and_epsilon_rates(array: List<String>) : List<String> {
  var rates_as_strings = mutableListOf("", "")

  for (i in 0..(array[0].count() - 1)) {
    var popular_bit = mutableMapOf("1".first() to 0, "0".first() to 0)
    array.forEach { item ->
      popular_bit.set(item[i], popular_bit[item[i]]!! + 1)
    }
    rates_as_strings.set(0, rates_as_strings[0] + if(popular_bit["1".first()]!! > popular_bit["0".first()]!!) "1" else "0")
    rates_as_strings.set(1, rates_as_strings[1] + if(popular_bit["1".first()]!! < popular_bit["0".first()]!!) "1" else "0")
  }

  return rates_as_strings
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n")
  val rates = binary_to_int(get_gamma_and_epsilon_rates(array))
  println(rates[0] * rates[1])
}