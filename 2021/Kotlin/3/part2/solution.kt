import java.io.File
import java.io.BufferedReader
import kotlin.math.pow

fun binary_to_int(rates: String) : Int {
  var converted = 0
  rates.reversed().forEachIndexed { j, bit ->
    converted += Character.getNumericValue(bit) * 2.0.pow(j).toInt()
  }
  return converted
}

fun get_oxygen_or_co2(array: List<String>, priority: List<Char>) : String {
  var new_array = array.toList()
  for (i in 0..(new_array[0].count() - 1)) {
    if (new_array.count() == 1) break

    var popular_bit = mutableMapOf("1".first() to 0, "0".first() to 0)
    new_array.forEach { item ->
      popular_bit.set(item[i], popular_bit[item[i]]!! + 1)
    }

    if(popular_bit["1".first()]!! >= popular_bit["0".first()]!!) {
      new_array = new_array.filter { it[i] == priority[0] }
    } else {
      new_array = new_array.filter { it[i] == priority[1] }
    }
  }
  return new_array.first()
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n")
  val oxygen_rate = binary_to_int(get_oxygen_or_co2(array, listOf('1', '0')))
  val co2_rate = binary_to_int(get_oxygen_or_co2(array, listOf('0', '1')))
  println(oxygen_rate * co2_rate)
}