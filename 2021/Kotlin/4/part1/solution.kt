import java.io.File
import java.io.BufferedReader
import kotlin.math.pow

fun calculate_value_of_board(board: List<List<MutableMap<Int, Boolean>>>, last_number: Int) : Int {
  var total = 0
  board.forEach { rows -> rows.forEach { cell -> cell.forEach { k, v ->
        if(!v) total += k
      }
    }
  }
  return total * last_number
}

fun check_for_winning_board(boards: List<List<List<MutableMap<Int, Boolean>>>>, last_number: Int) : Int {
  boards.forEach { board ->
    var win_cols = mutableMapOf(0 to true, 1 to true, 2 to true, 3 to true, 4 to true)
    board.forEachIndexed { i, rows ->
      var win_rows = mutableMapOf(0 to true, 1 to true, 2 to true, 3 to true, 4 to true)
      rows.forEachIndexed { j, cell ->
        cell.forEach { _, v ->
          win_cols[j] = win_cols[j]!! && v
          win_rows[i] = win_rows[i]!! && v
        }
      }
      if(win_rows[i]!!) return calculate_value_of_board(board, last_number)
    }
    if(win_cols.filter { (_, winning) -> winning }.any()) return calculate_value_of_board(board, last_number)
  }
  return 0
}

fun get_first_win_value(inputs: List<Int>, boards: List<List<List<MutableMap<Int, Boolean>>>>) : Int {
  inputs.forEach { number -> boards.forEach { board -> board.forEach { rows -> rows.forEach { cell ->
          if(cell[number] != null) cell[number] = true
        }
      }
    }
    var win_points = check_for_winning_board(boards, number)
    if(win_points != 0) return win_points
  }
  return 0
}

fun load_boards(array: List<String>) : List<List<List<MutableMap<Int, Boolean>>>> {
  val boards = array.filter { it != array.first() }
                    .map { it.split("\n")
                            .map { it.split(",")
                                      .map { it.split(" ")
                                              .map { it.replace("\\s".toRegex(), "") }
                                              .filter { it != "" }
                                              .map { mutableMapOf(it.toInt() to false)}
                                      }[0]
                            }
                    }
  return boards
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n\n")
  val inputs = array[0].split(",").map { it.toInt() }
  val boards = load_boards(array)
  println(get_first_win_value(inputs, boards))
}
