import java.io.File
import java.io.BufferedReader
import kotlin.math.pow

class Board(list: List<List<MutableMap<Int, Boolean>>>) {
  val board = list
  var completed = false
  var board_points = 0

  fun calculate_value_of_board(last_number: Int) : Int {
    if(completed) return board_points
    var total = 0
    board.forEach { rows -> rows.forEach { cell -> cell.forEach { k, v ->
          if(!v) total += k
        }
      }
    }
    completed = true
    board_points = total * last_number
    return board_points
  }
}

fun check_for_winning_board(boards: List<Board>, last_number: Int) : MutableList<Int> {
  var win_points = mutableListOf<Int>()
  boards.forEach { board ->
    var win_cols = mutableMapOf(0 to true, 1 to true, 2 to true, 3 to true, 4 to true)
    board.board.forEachIndexed { i, rows ->
      var win_rows = mutableMapOf(0 to true, 1 to true, 2 to true, 3 to true, 4 to true)
      rows.forEachIndexed { j, cell ->
        cell.forEach { _, v ->
          win_cols[j] = win_cols[j]!! && v
          win_rows[i] = win_rows[i]!! && v
        }
      }
      if(win_rows[i]!!) win_points.add(board.calculate_value_of_board(last_number))
    }
    if(win_cols.filter { (_, winning) -> winning }.any()) win_points.add(board.calculate_value_of_board(last_number))
  }
  return win_points
}

fun get_first_win_value(inputs: List<Int>, boards: MutableList<Board>) : Int {
  var last_win_points = 0
  inputs.forEach { number ->
    var boards_left = boards.filter { !it.completed }
    boards_left.forEach { board -> board.board.forEach { rows -> rows.forEach { cell ->
        if(cell[number] != null) cell[number] = true
        }
      }
    }
    var win_points = check_for_winning_board(boards_left, number)
    if(!win_points.isEmpty()) {
      if(boards.filter { !it.completed }.count() == 0) return win_points[0]
      else last_win_points = win_points[0]
    }
  }
  return last_win_points
}

fun load_boards_as_class(array: List<String>) : MutableList<Board> {
  val boards = array.filter { it != array.first() }
                    .map { it.split("\n")
                            .map { it.split(",")
                                      .map { it.split(" ")
                                              .map { it.replace("\\s".toRegex(), "") }
                                              .filter { it != "" }
                                              .map { mutableMapOf(it.toInt() to false)}
                                      }[0]
                            }
                    }
  return boards.map { Board(it) }.toMutableList()
}

fun main(args: Array<String>) {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n\n")
  val inputs = array[0].split(",").map { it.toInt() }
  val boards = load_boards_as_class(array)
  println(get_first_win_value(inputs, boards))
}
