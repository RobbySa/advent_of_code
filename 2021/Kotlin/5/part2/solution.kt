import java.io.File
import java.io.BufferedReader
import kotlin.math.pow
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf

class SeaFloorMapExpanded(array: List<List<List<Int>>>) {
  val sensor_map = array
  var dimensions = find_dimensions()
  var visual_map = build_visual_map()

  fun find_dimensions() : List<Int> {
    var largest_x = 0
    var largest_y = 0

    sensor_map.forEach { vent -> vent.forEach { pair ->
      largest_x = if(pair[0] > largest_x) pair[0] else largest_x
      largest_y = if(pair[1] > largest_y) pair[1] else largest_y
      }
    }
    return listOf(largest_x, largest_y)
  }

  fun build_visual_map() : MutableMap<Pair<Int, Int>, Int> {
    var visual_map: MutableMap<Pair<Int, Int>, Int> = mutableMapOf()
    for(x in 0..dimensions[0]) {
      for(y in 0..dimensions[1]) {
        visual_map[Pair(x, y)] = 0
      }
    }
    return visual_map
  }

  fun mark_vents_on_visual_map() {
    sensor_map.forEach { vent ->
      println(vent)
      if(check_if_linear_vent(vent)) {
        for(x in get_range(vent[0][0], vent[1][0])) {
          for(y in get_range(vent[0][1], vent[1][1])) {
            visual_map.set(Pair(x, y), visual_map[Pair(x, y)]!! + 1)
          }
        }
      } else {
        var start_x = vent[0][0]
        var start_y = vent[0][1]
        var x_incr = get_increment(vent[0][0], vent[1][0])
        var y_incr = get_increment(vent[0][1], vent[1][1])
        for(i in 0..get_difference(vent[0][0], vent[1][0])) {
          var new_x = start_x + (i * x_incr)
          var new_y = start_y + (i * y_incr)
          visual_map.set(Pair(new_x, new_y), visual_map[Pair(new_x, new_y)]!! + 1)
        }
      }
    }
  }

  fun get_increment(x1:Int, x2:Int) : Int {
    if(x1 - x2 < 0) return 1
    if(x1 - x2 > 0) return -1
    return 0
  }

  fun get_difference(x1:Int, x2:Int) : Int {
    return listOf(x1, x2).maxOrNull()!! - listOf(x1, x2).minOrNull()!!
  }

  fun get_range(x1:Int, x2:Int) : IntRange {
    return listOf(x1, x2).minOrNull()!!..listOf(x1, x2).maxOrNull()!!
  }

  fun check_if_linear_vent(vent: List<List<Int>>) : Boolean {
    if(vent[0][0] == vent[1][0]) return true
    if(vent[0][1] == vent[1][1]) return true
    return false
  }

  fun count_overlaps() : Int {
    var count = 0
    visual_map.forEach { _, value -> if(value > 1) count += 1 }
    return count
  }
}

fun main() {
  val file = File("../puzzle_input.txt").bufferedReader()
  val array = (file.use { it.readText() }).split("\n")
  val sea_floor_map = SeaFloorMapExpanded(array.map { it.split(" -> ").map { it.split(",").map { it.toInt() } } })
  sea_floor_map.mark_vents_on_visual_map()
  println(sea_floor_map.count_overlaps())
}
