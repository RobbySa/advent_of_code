import java.io.File
import java.io.BufferedReader
import kotlin.math.pow
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf

class Lanternfish(initial_state:Int) {
  var offspring_cycle = initial_state

  fun next_day() : Boolean {
    offspring_cycle -= 1

    if(offspring_cycle < 0) {
      offspring_cycle = 6
      return true
    }
    return false
  }
}

fun main() {
  val file = File("../puzzle_input.txt").bufferedReader()
  val lanternfish_array = (file.use { it.readText() }).split(",").map { Lanternfish(it.toInt()) }.toMutableList()

  for(day in 1..80) {
    var new_born_fishes = 0

    lanternfish_array.forEach { lanternfish ->
      if(lanternfish.next_day()) new_born_fishes += 1
    }

    for(fish in 1..new_born_fishes) {
      lanternfish_array.add(Lanternfish(8))
    }
  }
  println(lanternfish_array.count())
}
