import java.io.File
import java.io.BufferedReader
import kotlin.math.pow
import kotlin.collections.mutableListOf
import kotlin.collections.mutableMapOf

fun cycle_for_days(lanternfish_numbers_map:MutableMap<Int, Long>, days:Int) : MutableMap<Int, Long> {
  var new_map = (0..8).map { it to 0.toLong() }.toMap().toMutableMap()
  for(day in 1..days) {
    new_map = (0..8).map { it to 0.toLong() }.toMap().toMutableMap()
    lanternfish_numbers_map.forEach { k, v ->
      if(k == 0) {
        new_map.set(6, new_map[6]!! + v)
        new_map.set(8, new_map[8]!! + v)
      } else {
        new_map.set(k - 1, new_map[k - 1]!! + v)
      }
    }
    new_map.forEach { k, v ->
      lanternfish_numbers_map.set(k, v)
    }
  }
  return new_map
}

fun main() {
  val file = File("../puzzle_input.txt").bufferedReader()
  var lanternfish_numbers_map = (0..8).map { it to 0.toLong() }.toMap().toMutableMap()

  (file.use { it.readText() }).split(",").map { it.toInt() }.forEach { fish ->
    var current_count = lanternfish_numbers_map[fish]!!
    lanternfish_numbers_map.set(fish, current_count + 1)
  }

  println(cycle_for_days(lanternfish_numbers_map, 256).map { it.value }.sum())
}
