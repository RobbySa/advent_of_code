import java.io.File
import java.io.BufferedReader
import kotlin.math.abs

fun main() {
  val file = File("../puzzle_input.txt").bufferedReader()
  val position_array = (file.use { it.readText() }).split(",").map { it -> it.toInt() }.sorted()
  val median = position_array[position_array.count()/2]

  println(position_array.map { abs(it - median) }.sum())
}
