import java.io.File
import java.io.BufferedReader
import kotlin.math.abs

fun main() {
  val file = File("../puzzle_input.txt").bufferedReader()
  val position_array = (file.use { it.readText() }).split(",").map { it -> it.toInt() }.sorted()
  val average = position_array.sum() / position_array.count()

  println(position_array.map {
    var sum = 0
    for(step in 1..abs(it - average)) { sum += step }
    sum
  }.sum())
}
