file = File.read('./puzzle_input.txt')

# Part 1 find sum of calibration value
# On each line, the calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.
# For example:
# 1abc2 | pqr3stu8vwx | a1b2c3d4e5f | treb7uchet
# In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.

calibration_values = file.split.map do |line|
    numbers = line.delete("^0-9")
    "#{numbers[0]}#{numbers[-1]}".to_i
end

puts calibration_values.sum

# Part two correct to accept words as numbers
# It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".
# Equipped with this new information, you now need to find the real first and last digit on each line. For example:
# two1nine | eightwothree | abcone2threexyz | xtwone3four | 4nineeightseven2 | zoneight234 | 7pqrstsixteen
# In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.

new_calibration_values = file.split.map do |line|
    numbers = line.scan(/(?=(\d|one|two|three|four|five|six|seven|eight|nine|zero))/).flatten
    numbers = numbers.map do |number|
        number.sub("one", "1").sub("two", "2").sub("three", "3").sub("four", "4").sub("five", "5")
              .sub("six", "6").sub("seven", "7").sub("eight", "8").sub("nine", "9").sub("zero", "0")
    end
    "#{numbers[0]}#{numbers[-1]}".to_i
end

puts new_calibration_values.sum