# Extra functions
def check_game_validity(session_data)
    session_data.map do |cube_assortment|
        number_of_cubes, color = cube_assortment.split(' ')
        case color
        when 'red'
            number_of_cubes.to_i <= 12
        when 'green'
            number_of_cubes.to_i <= 13
        when 'blue'
            number_of_cubes.to_i <= 14
        end
    end.all?
end

def make_game_valid(session_data)
    min_red, min_green, min_blue = 0, 0, 0
    session_data.each do |cubes_drawn|
        cubes_drawn.each do |cubes|
            number_of_cubes, color = cubes.split(' ')
            case color
            when 'red'
                min_red = min_red > number_of_cubes.to_i ? min_red : number_of_cubes.to_i
            when 'green'
                min_green = min_green > number_of_cubes.to_i ? min_green : number_of_cubes.to_i
            when 'blue'
                min_blue = min_blue > number_of_cubes.to_i ? min_blue : number_of_cubes.to_i
            end
        end
    end
    [min_red, min_green, min_blue]
end

file = File.read('./puzzle_input.txt').split("\n")

# Part 1 find the sums of the game ids which are impossible
# The Elf would first like to know which games would have been possible if the bag contained only 12 red cubes, 13 green cubes, and 14 blue cubes?
# For example:
# Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
# Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
# Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
# Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
# Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
# In this example, the games 3 and 4 would be impossible because in game 2: 20 red cubes were shown and in game 4: 15 blue cubes were shown.

games = file.map do |game|
    first_pass = game.match(/Game (\d+): (.*)/)
    game_id = first_pass[1].to_i
    game_data = first_pass[2].split('; ').map { |session| session.split(', ') }
    game_validity_array = game_data.map { |session| check_game_validity(session) }

    { game_id: game_id, game_data: game_data, game_legal: game_validity_array.all? }
end

puts games.select { |game| game[:game_legal] }.sum { |game| game[:game_id] }

# Part two find sum of power of all game sets
# The power of a set of cubes is equal to the numbers of red, green, and blue, required
# for the games to be legal, cubes multiplied together. The power of the minimum set of
# cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630, and 36, respectively.
# Adding up these five powers produces the sum 2286.

games_power = games.map do |game|
    min_cubes_required = make_game_valid(game[:game_data])

    power = min_cubes_required.inject(:*)
end

puts games_power.sum
