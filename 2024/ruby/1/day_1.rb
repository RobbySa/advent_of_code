file = File.read('./2024/ruby/1/puzzle_input.txt')

# Part 1 find sum of distances
# Within each pair, figure out how far apart the two numbers are; you'll need to add up all of those distances.
# For example, if you pair up a 3 from the left list with a 7 from the right list, the distance apart is 4;
# if you pair up a 9 with a 3, the distance apart is 6.

list_1, list_2 = [], []
file.split("\n").each do |line|
    pair = line.split('   ')
    list_1.append(pair[0].to_i)
    list_2.append(pair[1].to_i)
end

transposed_list = [list_1.sort, list_2.sort].transpose

puts transposed_list.map { |x, y| (x - y).abs }.sum

# Part two calculate similarity score
# This time, you'll need to figure out exactly how often each number from the left list appears in the right list.
# Calculate a total similarity score by adding up each number in the left list after multiplying it by the number
# of times that number appears in the right list.

similarity_score = 0
list_1.each_with_index do |x, i|
    similarity_score += x * list_2.count(x)
end

puts similarity_score