file = File.read('./2024/ruby/2/puzzle_input.txt')

# Part 1 find how many reports are safe
# The engineers are trying to figure out which reports are safe.
# The Red-Nosed reactor safety systems can only tolerate levels that are either gradually increasing or gradually decreasing.
# So, a report only counts as safe if both of the following are true:
#  - The levels are either all increasing or all decreasing.
#  - Any two adjacent levels differ by at least one and at most three.

def is_safe?(report)
    increasing = report[0] < report[1]
    report.each_cons(2).all? do |v1, v2|
        v1 != v2 && v1 < v2 == increasing && (v1 - v2).abs.between?(1,3)
    end
end

safe_reports = 0

file.split("\n").each do |line|
    report = line.split(' ').map(&:to_i)
    safe_reports += 1 if is_safe?(report)
end

puts safe_reports

# Part two find how many reports are nearly safe
# This is exactly the same as part 1 but one number can be removed from the list to make it work

def check_alternative_permutations(report)
    report.each_with_index do |_v, i|
      if is_safe?(report.reject.with_index{ |_v2, j| j == i })
        return true
      end
    end

    return false
end

near_safe_reports = 0

file.split("\n").each do |line|
    report = line.split(' ').map(&:to_i)
    if is_safe?(report)
        near_safe_reports += 1
    elsif check_alternative_permutations(report)
        near_safe_reports += 1
    end
end

puts near_safe_reports