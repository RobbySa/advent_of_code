file = File.read('./2024/ruby/3/puzzle_input.txt')

# Part 1 find result of all multiplications
# It seems like the goal of the program is just to multiply some numbers.
# It does that with instructions like mul(X,Y), where X and Y are each 1-3 digit numbers.
# For instance, mul(44,46) multiplies 44 by 46 to get a result of 2024. Similarly, mul(123,4) would multiply 123 by 4.

# However, because the program's memory has been corrupted, there are also many invalid characters that should be ignored,
# even if they look like part of a mul instruction. Sequences like mul(4*, mul(6,9!, ?(12,34), or mul ( 2 , 4 ) do nothing.

total = 0

file.split("\n").each do |line|
    matches = line.scan(/mul\(\d*,\d*\)/)
    matches.each do |match| 
        number_list = match.gsub(/[^\d]/, ' ').strip.split(' ')
        total += number_list[0].to_i * number_list[1].to_i
    end
end

puts total

# Part two find result of all allowed multiplications
# This corrupted memory is similar to the example from before
# but this time the mul(5,5) and mul(11,8) instructions are disabled because there is a don't() instruction before them.
# The other mul instructions function normally, including the one at the end that gets re-enabled by a do() instruction.

new_total = 0
enable = true

file.split("\n").each do |line|
    matches = line.scan(/mul\(\d*,\d*\)|don't|do/)
    matches.each do |match|
        if match == "do"
            enable = true
        elsif match == "don't"
            enable = false
        else
            number_list = match.gsub(/[^\d]/, ' ').strip.split(' ')
            new_total += number_list[0].to_i * number_list[1].to_i if enable
        end
    end
end

puts new_total