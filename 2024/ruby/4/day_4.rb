file = File.read('./2024/ruby/4/puzzle_input.txt')

# Part 1 find all xmas
# This word search allows words to be horizontal, vertical, diagonal, written backwards, or even overlapping other words.
# It's a little unusual, though, as you don't merely need to find one instance of XMAS - you need to find all of them.

def found_in_direction(row, col, vertical_mov, horizontal_mov)
  word = ""
  
  (0...4).each do |increment|
    new_row = row + (increment * vertical_mov)
    new_col = col + (increment * horizontal_mov)

    break if new_row < 0 || new_col < 0 || new_row > $rows - 1 || new_col > $cols - 1

    word << $w_search[new_row][new_col]
  end

  $total += 1 if word == "XMAS" || word == "SAMX"
end

$total = 0

$w_search = []
file.split("\n").each { |line| $w_search.append(line.chars) }

$rows = $w_search.size
$cols = $w_search.first.size

(0...$rows).each do |row|
  (0...$cols).each do |col|
    found_in_direction(row, col, 0, 1)
    found_in_direction(row, col, 1, 0)
    found_in_direction(row, col, 1, 1)
    found_in_direction(row, col, 1, -1)
  end
end

puts $total

# Part two find result of all allowed multiplications
# Looking for the instructions, you flip over the word search to find that this isn't actually an XMAS puzzle;
# it's an X-MAS puzzle in which you're supposed to find two MAS in the shape of an X. One way to achieve that is like this:
# M - S
# - A -
# M - S

new_total = 0

(0...$rows - 2).each do |row|
  (0...$cols - 2).each do |col|
    word1 = "#{$w_search[row][col]}#{$w_search[row + 1][col + 1]}#{$w_search[row + 2][col + 2]}"
    word2 = "#{$w_search[row + 2][col]}#{$w_search[row + 1][col + 1]}#{$w_search[row][col + 2]}"
    
    next if word1 != "MAS" && word1 != "SAM"
    next if word2 != "MAS" && word2 != "SAM"
  
    new_total += 1
  end
end

puts new_total